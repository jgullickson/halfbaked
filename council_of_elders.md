# Council of Elders

A band of old musicians sharing song about the things they've learned the hard way.

How I imagine this is a group of weirdos each taking a turn "directing" a song with a story, resulting in a collection of weird, diverse songs.  

## Goals

* Have a few practics/jam sessions
* Write a few songs
* Record an album
* Play a show

The number and duration of jam sessions is hard to determine, but needs to be enough to result in recording enough songs for an album.

The songs can be in any style and are ideally in diverse styles.  The only requirement is that each member leads the process of at least one song, and that each song conveys something important from the person directing it.

The album could be released as part of the RPM Challenge.

The show could be a New Year's Eve show at the Albion town hall.

It's worth pointing-out that the musicians don't necisarilly *have* to be in the same physical location for this, and it might be fun and extra weird to have someone play the show using some weird telepresense process.

## Candidates

* Jason
* Matt
* Derek
* Tone
* Joe
* Andy
* Brad
* Marc
* ...
