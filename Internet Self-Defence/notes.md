# Internet Self-Defense

## Goals

*  Help normal people understand why free and open Internet access is important
*  

## Outline

1.  Who is this course for?
2.  Why do good guys need privacy?
  1.  Everybody is somebody's bad guy
3.  How the Internet works (in 10 minutes)
4.  Threat modeling
5.  Actors
  1.  Good actors
  2.  Bad actors
  3.  State actors
6.  What could possiblie go wrong?
7.  


## Misc. topics/suggestions

*  Duckduckgo instead of Google
*  Mailinabox instead of Gmail
*  NextCloud instead of Google Docs, etc.
*  Tails Linux (https://tails.boum.org/)
  * Might be cool to hand out Tails flash drives...
*  Tor and Tor devices (Anonabox, etc.)
*  VPNs
