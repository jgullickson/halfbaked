# The long way home

Next Saturday I'm assembling a trailer with my brother in Edgerton.  The trailer is only rated for 55MPH, and while some have claimed to have safely towed the same trailer cross-country at 70MPH+, I'm not terribly interested in proving the manufacturer wrong.

Plus, I'm just not in that much of a hurry.

It's only about 50 miles from Edgerton to my home in Beaver Dam, but the usual route consists of either 90% or 20% Interstate highway, where the posted limit is almost exclusively 70MPH.  I figured I'd have to settle for doing 60 in the right lane for the last 15 miles or so of the trip, but decided to dig out a map and see if there was an alternative.

Turns out there is.  It's a little longer in time but a little shorter in miles, and it's 55MPH or less the whole way home.  Just what I'm looking for.  It's also almost entirely free of cities or towns which as long as nothing unexpected comes up, makes for the best driving.

In order to avoid the crowds on Saturday, I'm driving down to Janesville tonight (Tuesday) to pick-up the trailer kit with my brother (he has a truck; this trailer business is a bit of a chicken-and-egg problem).  We'll pick-up the kit, drop it at his place where it will await assembly on Saturday morning.

Since I will be making an extra trip to Edgerton I'll have an opportunit to scout this new route before I'm towing a trailer for the first time.  I do like adventure and surprises, but since I'll be driving Jamie's new Jeep, I'd like to avoid any catestrophies.

[Click here to view the route](https://www.google.com/maps/dir/Edgerton,+Wisconsin/412+York+St,+Beaver+Dam,+WI+53916,+USA/@43.264176,-89.0910076,12z/data=!4m14!4m13!1m5!1m1!1s0x8806150e78241027:0x47d9f38e19264e49!2m2!1d-89.0676125!2d42.8352835!1m5!1m1!1s0x88069f6e332b3c6b:0xd3e69532b54a9018!2m2!1d-88.8366659!2d43.4658019!5i2)

## Tuesday

The first leg of the route is familiar territory for me.  Leaving Edgerton on Highway 51, the route passes-through Albion before connecting to Highway 73.  From there it's mostly country highway other than passing through Deefiefled (pop. XXX) and then Marshall (pop. XXX).  I've driven this route hundreds if not thousands of times, typically following 73 all the way up to Columbus where it crosses-under 151, which I then take to Beaver Dam.

The new route takes 73 out of Marshall but then departs East on to Highway TV, and I don't think I've ever been on that highway before.

I didn't print the map before I headed out, because I thought I could trick the Jeep's nav into re-creating the route.  I was wrong.  The best I could do was get a route that took the back-roads to Columbus, but from there it directed me back onto 151.  For the next run, with trailer in tow, I'm going to need a printed map.

The stretch between Marshall and Columbus was enjoyable though, and once I turned on to Highway 89 from TV it began to look familiar.  After awhile I realized that I had been on this road before; it's a great stretch of road with a few 40-45MPH curves that sweep through the countryside.

The Jeep has satallite radio, it's not something I would deliberately pay for, but it came with a 1-year subscription so I'll use it when I don't have another source of music handy.  There is an ironically-named "Classic Vinyl" channel that occasionally turns-up something old-but-new-to-me, and today it introduced me to "Traffic".  Contrary to what the name would lead you to beleive, it was good driving music and went well with the winding roads leading to Columbus.

This time of year you can run comfortably with the windows down, and this is my prefered method of travel.  You're a little more in-touch with your surroundings, and it adds another dimension to the experience (especially after re-breathing filtered air all winter).  For the most part the smells are what you would expect from a drive in the country, moist soil, cut greens and the occasional whif of manure, but there was something else in the air today that I can't quite put my finger on, a scent that is attached to a memory that I can't quite retrieve.

As I'm thinking about this the Traffic song (insert name) winds-down and I arrive in Columbus.  A few minutes later I'm heading-down the on-ramp to Highway 151 and the last leg of today's ride is superslab all the way.

## Saturday
