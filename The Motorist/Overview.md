# The Motorist

A quarterly 'zine for people who love driving.

## Colummns

*  Editorials
*  Letters
*  First Rides
*  Driving Tunes
*  Day Trips
*  Travel Hacks
*  Destinations
*  Books
*  Gifts
*  Road Trip Journals
*  Epic Roads
*  Special Events

## Notes

Print and online editions.  Current issue avaliable in print first, articles from each edition are released online for free one month later (subscribers can read the current edition online as soon as it's released).

A podcast may accompany each issue (most likely an audio version of a road trip journal or special event).

motoristzine.com
