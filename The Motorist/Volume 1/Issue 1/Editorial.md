# Editorial

## Introducing The Motorist

In terms of getting from point A to point B, automobiles are pretty rediculous.  Most people use cars to drive to work, drive to school or drive somewhere for some reason.  There are much better ways to move people around for these purposes, or better yet, don't move them at all*.

The Motorist isn't for those people.

The Motorist is for people who like to drive.  Here you will find stories, tips and other information about driving for pleasure.  Hopefully it will inspire you to go for a ride "for no good reason", and enjoy it regardless of what car or what roads or what climate you have at your disposal.



- Jason
