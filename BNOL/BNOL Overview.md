Be Nice Or Leave (BNOL) was started as a response to the frustration I felt with existing social networks and an interest in returning to the embryonic joy of Twitter in its early form.

The core tenent of BNOL is a social construct, not a technological one: create a place to share nice things, save the hostility, harrassment and negativity for elsewhere.  This *behavioral* goal was above all others, and as such the choice of implementation technology, architecture, etc. was all pushed aside in favor of quickly constructing an environment to house the social behavioral construct.

There is nothing about BNOL itself that enforces this social constraint.  I deliberately avoided this because almost every technological solution to this problem has side-effects.  Instead, it's assumed that people who bother to use the system will behave, and if they don't, we'll handle it as a team. 
