A lone SBC connected to the Internet, offering to provide service for a modest fee.  Promiscuously connecting to other networks when avaliable, and capable of reproducing itself through both automated configuration as well as physical procurement of parts.

Ideally the device is as autonomous as possible, utilizing protocols that can "make do" with whatever connectivity is avaliable (things that don't require firewall configuration, for example).
