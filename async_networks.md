# Asynchronous Networks

Many of the problems we have with the current Internet can be solved by changing from the always-on nature of the way we currently use the Internet to applications which work well using asynchronous modes of communication.  This change would preserve many of the features and benefits of the current Internet and extend these benefits to a much larger number of people.  At the same time this change will reduce the cost of network infrastructure and make it possible for the network to function without interference by private or public parties, decrease surveillance, increase privacy and reduce energy consumption as well.

## Topics

* Mesh networks
* Store-and-forward
* Podcasting
* Bittorrent
* Data Mules
* Personal Computers
* Strategies for inherently synchronous applications (realtime communications, etc.)

### Mesh Networks

Most user's relationship with the Internet is mediated by an ISP, however this was not the original intention of the Internet.  The Internet was intended to be a network of peers connected together, on equal footing with the agreement that as a condition of being connected they would allow traffic from other peers to flow through their network.  Mesh networks are a return to this idea, creating an Internet out of a network composed of peer nodes passing data along from source to destination.

Wireless mesh networks can be built from many components, including commodity networking hardware and personal computers.  Using existing free software, these inexpensive components can be used to create networks spanning kilometer distances without expensive dedicated lines.

Most mesh networks use the same applications and protocols as the current Internet but due to their nature mesh networks typically have worse performance.  However if protocols and applications designed for a mesh network were used, performance can meet or exceed that of a "traditional" Internet connection.

### Store-and-forward

Store-and-forward applications are not new, in fact the most familiar example is Internet e-mail.  Instead of using an always-on network to connect the user's computer to an application running on a server, a store-and-forward application runs on the user's computer and stores the information the user provides locally.  When a connection becomes available, the user's computer connects to the destination computer and transmits the message.

While this point-to-point communication is good, it becomes very powerful when a network of personal computers cooperate to deliver these messages.  For example, a user creates a document on their personal computer and sends it to a colleague.  Instead of waiting until the colleague's computer is available, the original computer only need wait until a computer participating in the network becomes available.  The message can then be sent to this intermediary system which will in turn pass the message along to another computer and then another until the final destination is available.  This allows messages to be passed between computers that are never connected to the network at the same time, or that are great distances apart with no direct connection between them.

This is exactly how Internet email was originally designed to work.  Web-based email has broken this model for convenience and commercialization reasons, but a return to store-and-forward for email has no impact on the user's experience and would dramatically reduce the amount of connectivity required to support email communications.

Other applications could also adopt this method of communication and further reduce the need for high-bandwidth, always-on Internet connections.  To some extent network equipment already works this way, but the amount of data they can store and the amount of time they store it for is infinitesimal.  It requires cooperation between both the network and the application to maximize the benefits of the store-and-forward architecture.

### Podcasting

Podcasting was originally developed as a way for users to share large media files without access to large amounts of server storage space or large amounts of Internet bandwidth.  In the intervening years it has mostly become just another form of streaming media which is not ideal for asynchronous networks, but in its original form it was ideal.

Similar to store-and-forward, a podcast consists of two parts: a collection of large media files and a small file describing the media.  Users who subscribe to the podcast first download this small file which can be transmitted with almost no connectivity at all (typically less than a kilobyte of data).  Once this file is received, the user's personal computer can download the media files opportunistically when connectivity is available.  This process is automated so it can happen without the user's intervention (or even awareness).  Clever podcasting software can even manage partial downloads of the large media files when connectivity is particularly intermittent.  Since the files are stored locally they can be played-back regardless of whether or not a network connection is available, and without interruption and quality issues associated with streaming media.

### Bittorrent

Bittorrent is heavily associated with piracy, but that's unfortunate because the underlying protocol is one of the most efficient ways to share large amounts of data between computers.  Instead of one user downloading a large file from one server, bittorrent allows the user to download parts of the file from any computer that has a copy of the same file.  The result is that performance *increases* the with the number of users who download the file (which is the opposite of what happens when a single server hosts the whole file).  This can also reduce the time it takes to download the file because it can be delivered by computers that are closer to the user's on the network.

The fact that sharing files via bittorrent isn't built into every personal computer is a shame, and the reason is most likely due to its association with piracy.  


### Data Mules

Data mules are network nodes just like the computers describe above with one key difference: they move around in physical space.  The most common example is that of having a mule installed on a bus: as the bus moves through town (or between towns), computers connect to the mule and transfer data.  As the bus goes in-and-out of range, data is moved from computer to computer.  This allows for high-bandwidth, high-latency connections between physical locations that are otherwise disconnected.  Combined with store-and-forward application designs, data mules can provide substantial effective connectivity.

The bus example is a good one, but consider the fact that so many of us carry a computer in our pockets everywhere we go.  If these computers functioned as mules, the effective connectivity between any two points on the globe would grow exponentially.

### Personal Computers

Personal computers are mentioned repeatedly in this piece and it might sound wordy or archaic to use this terminology.  However I think it's important to place emphasis on the idea that participants in asynchronous networks use devices that can honestly be called general-purpose computers, and that these computers are user's personal possessions.  General-purpose is meant to imply that the computer can be used for any application, and that includes programming the computer itself.  A general-purpose computer should be self-hosting in the sense that it is capable of modifying and compiling all of the software (and ideally, hardware) that it uses.  It should be personal in that it can be trusted with the personal information of the user, and that access to it should be exclusively under the user's control.  Such a computer should be worthy of trust, and the user should be able to assume that any information they store on the computer will be safe, and no information will leave the computer without their explicit approval.

Conversely the user can assume that information leaving their computer is inherently *not-secure*, and that anything they share on the network will require explicit measures to be taken to ensure privacy.  This might sound like a step backward, however this fact is already true of existing Internet applications, but large amounts of resources are consumed to convince users otherwise.  The asynchronous network discards this wasteful illusion of security in favor of providing users with a computer they can trust and tools to communicate securely of their choosing.

### Strategies for inherently synchronous applications


# References

* https://solar.lowtechmagazine.com/2015/10/how-to-build-a-low-tech-internet.html