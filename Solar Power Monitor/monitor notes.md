# Solar Power Monitor

A basic monitor to measure solar power collected and consumed.

## Components

*  Arduino-compatible with networking (something "fire" board?)
*  ACS712 20A Current sensor
*  ??? resistor
*  ??? resistor
*  LCD character display

## Theory of operation

## Math

## Code

## References
*  https://learn.sparkfun.com/tutorials/acs712-low-current-sensor-hookup-guide
*  https://forum.mysensors.org/topic/3840/12v-solar-battery-monitor/10
*  http://www.allegromicro.com/en/Products/Current-Sensor-ICs/Zero-To-Fifty-Amp-Integrated-Conductor-Sensor-ICs/ACS712.aspx
*  http://www.instructables.com/id/How-to-Measure-a-Solar-Cells-Power-Output/
*  http://www.instructables.com/id/ARDUINO-ENERGY-METER/
