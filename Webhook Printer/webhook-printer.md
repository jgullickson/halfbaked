# Webhook Printer

A hardware webhook target that turns webhook payloads into hardcopy documents.

# References

* https://github.com/tojocky/node-printer
