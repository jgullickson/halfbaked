PI_HEIGHT = 85;
PI_DEPTH = 56;
PI_WIDTH = 25; //16;
WALL = 3;

// bottom box
difference(){
  union(){
    cube([PI_WIDTH + (WALL * 2), PI_DEPTH + (WALL * 2), PI_HEIGHT]);
    // feet
    translate([-(PI_WIDTH + (WALL * 2)) / 2, 0, 0]){
      cube([PI_DEPTH + (WALL * 2), PI_DEPTH / 4, WALL]);
      translate([0,PI_DEPTH - (PI_DEPTH / 4),0]){
        cube([PI_DEPTH + (WALL * 2), PI_DEPTH / 4, WALL]);
      }
    }
    // face
    translate([0, - (PI_WIDTH / 4), 0]){
      cube([(PI_WIDTH + (WALL * 2)) / 3, PI_WIDTH / 2, PI_HEIGHT]);
    }
    translate([(PI_WIDTH + (WALL * 2)) / 2, 0, 0]){
      cylinder(r=PI_WIDTH / 4, h=PI_HEIGHT, $fn=50);
    }
    translate([(PI_WIDTH + (WALL * 2)) / 2, -PI_WIDTH / 4, 0]){
      cube([(PI_WIDTH + (WALL * 2)) / 2, PI_WIDTH / 2, PI_HEIGHT]);
    }
  }  
  // pi cavity
  translate([WALL, WALL, WALL]){
    cube([PI_WIDTH, PI_DEPTH, PI_HEIGHT]);
  }
  // LED openings
  translate([(PI_WIDTH + (WALL * 2)) / 2, -(PI_WIDTH / 3), (PI_HEIGHT / 3)]){
    cube([5,PI_DEPTH + (WALL * 2),2]);
    translate([0,0,10]){
      cube([5,PI_DEPTH + (WALL * 2),2]);
      translate([0,0,10]){
        cube([5,PI_DEPTH + (WALL * 2),2]);
        translate([0,0,10]){
          cube([5,PI_DEPTH + (WALL * 2),2]);
          translate([0,0,10]){
            cube([5,PI_DEPTH + (WALL * 2),2]);
          }
        }
      }
    }
  }
  // power in
  translate([WALL, WALL + PI_DEPTH - 1, WALL + 5]){
    cube([10,WALL + 2, 10]);
  }
  // audio out
  translate([WALL, WALL + PI_DEPTH -1, WALL + 50]){
    cube([15,WALL + 2, 15]);
  }
}
/*
// top box
translate([0,-(PI_WIDTH)/4,PI_HEIGHT]){
  difference(){
    cube([PI_WIDTH + (WALL * 2), PI_DEPTH + (WALL * 2) + (PI_WIDTH / 4), PI_HEIGHT / 4]);
 
    // power switch
    translate([WALL, -1, PI_HEIGHT / 16]){
      cube([WALL, WALL * 2, WALL * 2]);
    }
    // drives
    translate([(PI_WIDTH + (WALL * 2)) / 3, -1, WALL]){
     cube([(PI_WIDTH + (WALL * 3)) / 2, WALL, (PI_HEIGHT / 4) - (WALL * 2)]);
    }
    
    // pi cavity
    translate([WALL, WALL + (PI_WIDTH)/4, -1]){
      cube([PI_WIDTH, PI_DEPTH, PI_HEIGHT / 4]);
    }
  }
}
*/
