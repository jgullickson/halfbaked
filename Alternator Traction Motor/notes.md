# NeedsAName

A project to design a standard, repeatable way to recycle automotive alternators into an alternative to the ubiquitous Briggs & Stratton/etc. 5HP gasoline engine.

## Overview

Sturdy electric motors are expensive and somewhat hard to come by.  Automotive alternators are cheap and plentiful.  It's possible to convert an automotive alternator into a motor, but I haven't found a go-to reference for doing so and achieving the best results.

The goal of this project is to discover the best way to utilize automotive alternators as traction motors and document the process.  The output from this process will be a standardized procedure for modifying the alternator and will describe the support system necessary to drive it as a motor.

As a reference the common 5HP gasoline engine is used, because creating an alternative to this power unit immediately creates a large number of applications for this electric motor, and establishes a framework to ensure that the process and output created can be easily put to work by others.  It also reduces the chances of creating a process or system which only applies to a limited application, or is better served by existing designs.


## Log

### 08142017

Resurrecting this project as I've recently become infatuated with the [Super 73](https://www.kickstarter.com/projects/lithiumcycles/the-super-73) e-bike, and this motor could be an ideal part of building something similar at a much lower cost.

Revisited the [e-kart](https://hackaday.io/project/12978-e-kart-the-electric-go-kart) project on Hackaday.io (they are using an alternator motor as well)to see if there's been any updates since the last time I checked on it.  Didn't see anything new, but reached-out to the creators to see if I could get more details from them about their experiments and any unpublished updates that may be available.

A good next step might be to try driving the alternator again using the cheap brushless ESC from previous experiments but using wire of sufficient gauge (last time it melted, use 12 AWG this time).  Also the e-kart team suggested powering the field/exciter with 8VDC and reducing the voltage as motor RPM increases.



## References

* https://hackaday.io/project/12978-e-kart-the-electric-go-kart
* https://www.kickstarter.com/projects/lithiumcycles/the-super-73
* https://toolclerk.com/products/controller-48v-72v-1500w-brushless-motor
* https://www.instructables.com/id/Make-Your-Own-ESC
* https://www.digikey.com/en/articles/techzone/2013/mar/an-introduction-to-brushless-dc-motor-control
* https://www.digikey.com/en/articles/techzone/2013/jun/controlling-sensorless-bldc-motors-via-back-emf
* https://www.cityofmadison.com/trafficEngineering/documents/ElecBikeLawsWeb20130520v.pdf