
## Overview

A WiFi and cloud service interface for the TRS-80 Model 100 built around the ESP8266 module.

The system consists of a hardware module which connects to the M100's RS-232C serial interface and software for both the module and the M100.  

Software on the module translates data stored in cloud services into formats and sizes usable by the M100.  Software that runs on the M100 provides a user interface to the software running on the module (including configuration).

## Hardware

*  ESP8266
*  MAX-232
*  DB-25
*  

## Software

Software running on the module needs to be able to make HTTP(S) connections to cloud services, perform authentication and buffer data in order to work within the constraints of the M100's capabilities.  

The priciple application will be to browse, upload and download text-based files from Nextcloud.

## Status

Initial thoughts on implementation and goals have been recorded.  

### TODO

*  


## References

*  https://www.sparkfun.com/tutorials/215