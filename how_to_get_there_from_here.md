# How to get there from here?

As an anarchist I'm interested in autonomy.  This means eliminating the means of control used by authority.  For me that's primarily indirect violence via capitalism, so my primary objective is to remove the things I need to survive from being mediated via capitalism.

Ultimately in order for this to work on any human scale it requires collaboration, but it's easier to get people on-board when you can show them something that works.  So start small.

Start with your own home, or even a single room in your home.  Study the space and identify what's required to support it (heat, electricity, etc.).  Examine how these requirements are bound by capitalism and find ways to meet these needs without that dependency.  Invite others into these spaces and encourage them to enjoy the autonomy they provide, in the spirit of Hakim Bay's Temporary Autonomous Zones (TAZ).

This sounds kind of abstract so maybe a concrete example will help:

(insert example here)

Once you've achieved autonomy in one space (no matter how small), the next step is to expand the area of that space by applying the same process to an adjacent space.  If you've liberated one room, liberate the one next to it; if you've liberated a household, work on your neighbor next.  Once you have a square of autonomy to anchor yourself to, the next square should be easier, then the next, and so on.

There will undoubtedly be setbacks, and exponential growth should not be expected.  Nor should unchecked growth be desirable; if a space resists autonomy forcing it to become autonomous is just another form of tyranny.  Instead grow around these spaces.

# References/Ideas

* Conway's Game of Life (could be used to simulate growth of autonomous spaces?)
* The Golden Ratio