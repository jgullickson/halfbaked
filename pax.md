# PAX - Personal Assistant X

Notes from an early morning:

* Conversational email interface ("do you have this file?"/"Here's some matches"/"Send me the third one")
* Video annotated using computer vision on each frame to identify objects, people, text, etc. and count frequency, proximity, etc.
* Personal Assistant X, consumes and indexes all content you create, proxies account access, etc.
* Creates a composite timeline across all activiteies (apps, devices, etc.)
* Parts: archive, search engine, journal, classifiers, account interfaces, analytics
* Book reader (or interface into existing reader provided sufficient API) adds books to library, logs reading time in the journal, captures highlights, bookmarks, etc.
* Capture media in -> mood out relationships (reactions to posts, etc.)
* Intervention prompts ("how are you doing?", "are you OK?", etc.)
* A desklamp (or just bulb?) as an interface to physical media (image capture, video/audio recording, other physical data)
* vector capture pen/marker