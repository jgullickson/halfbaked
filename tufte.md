* Could lightfield photography increase the data resolution and allow the consumer more control?
* Robert Venturi - Complexity and Contradiction in Architecture
* Is minimalism based on fear?

"design reasoning must correspond to scientific reasoning."

[] See if Murfie still uses JSFS and if I can access the metadata

* What the problem is
* Why the problem is important
* What the solution to the problem is
  * Repetition, variations on a theme
  * Materials, printed and practiced
    * PGP: Particular, General, Particular
  * Show-up early, finish early
    * Quit before they've had enough
* Calculate image substance (%)
  * The proportion of space on the screen devoted to content vs. admin, etc.
  * character counts
  * number of controls
* The information becomes the interface
* Hardcopy still matters
* "visual confections"
* Avoid contempt for your audience

"The efficient communication of complex quantitive ideas."

[] Investigate using high-resolution maps to view and identify patters and bottlenecks in High-Performance computers

"data paragraph"
"little data"

* Let the viewer decide
[] check out th enational weather service website
* Present as much data as possible, agacent in space, don't bury it in the deck
* Kill the slow reveal
[] Measure the "datarate" of a meeting
* Eliminate contempt for audience and content
* The audience is not your alone, don't worry about consistency in design
* Clean design, rich content
* Reason from content
* Everything is subordinate to content
[] Check out "music animation machine"
* "Sparklines"
[] find out if any editor displays sparklines inline with text
* Cat-like embedded processing could embed dynamic sparklines
* "Simplification" of data is lossy compression
* "diamonds in the swamp"
* Allow audience to scan and select
* High rez and touchscreen
* Focus on a spacial ajacency
* Direct label when possible
[] Dress with some contrast
[] Review The New York Times "policy stories"
* Attribution, responsibility, reputation
* Repetition = Reputation
[] Use ML to quanitfy numbers within text (how much Q...?)
* Information is not constrained to its means of production
* Use a quote from an expert
  * Shows you are not alone in your ideas
  * use both supporting and contrary quotes
* Use sources
* **Analytical Thinking** is non domain-specific
[] Revisit "Jefferson Desk"
* Illuminate error bounds
* Sources should be linked

"to have an open mind but not an empty head"

* How does the model work?
* Most things are multivariate
[] Why most published studies are false (look this up)
* Don't use vauge terms like "significant", "conservative", etc.
[] Research undo stack for CMS
* Analyticial thinking -> analytical design
* Reasoning about Content
  * P 124 in green book
* "Compared with what?"
  * show causality
  * show multivariate dimensions
    * 3 or more
    * completely integrated
* Reject fashion
* Assume equality
  * intelligence
  * quality of motive
  * Do whatever it takes to convey the content
* Document the evidence, make documentation avaliable
  * take responsibility
* the goal is to get closer to the truth 
* content above all else

"to clarify, add detail"

I really need to work on eliminating contempt for my audience, content, etc.

* "Teaching to see" film
[] Check-out "Origin" scientific data software

"The meaning of life is seeing and learning"

* Presentations
  * Content
  * Credibility
    * Don't lie
    * document
    * demonstrate local detail
    * know the source of your data
      * "sampling to please"
  * practice
    * rehersal
    * video
    * audio-only
  * You don't need to know your audience
    * treat them as you want to be treated
  * Begin with a document & study hall
    * document on paper
    * don't draw attention to paper
    * 3.5 min per page
  * timing
    * show up early
    * end early
  [] consider an entire barcamp using this method
  [] Research Ballmer & Powerpoint
  [] Try this at the doctor
    * leave margin for notes
  [] Try describing the new technique as software
  [] Researching screening tests
    * Loannidis (false research)
  * make a sort of "program" for really big presentations
  * consider presentor "metadata", filmstrip as "preroll"
  * Annotated linking lines to establish and call to attention to causality
  * Causation lines reveal nature (non-hierarchical relationships)
  * Founding document of the web: 6 pages
    * "vauge but exciting"
  [] research why we keep falling back into heierarchy

"...the method of storage must not place its pown restraints on the information"

  [] experiment with layers

If you know what you're talking about, you can teach it to anyone; if you can't teach it, you don't really know it.

Wow, my brain is exploding

I have 8 pages of notes already.  It's really hard to think this much w/o someone to talk to about it.

"Concepts that explain everything explain nothing." - P 149 green book

* Measure graphs by throughput
[] See if CERN is hiring
* Anti-glare, dark room
* Macro-micro echos Raskin's zoomable UI
* Could chart data be "played" with generative music?
* "don't segregate info by mode of production" (another Rasking topic)
* PLOS

* Shape-up data analysis
  * Treat statistical lives as real lives
  * Dr. Andrew Vickers
  * Add adverse effects to the lable of standard statistics models
  * "Dustbowl empericicim"
  * Avoid model hacking (overfitting)
  * SMR (multiple regression) problems
  * Khon Sari heart surgery book

[] look into Retraction Syndrome
[] Can you calculate a credibility rating
[] Berty art typeography?
* Ausonio tree of reflections
* sordonic

**How do (you/I/they) know that?**

