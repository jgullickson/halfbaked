# #DieEvernoteDie

I loved Evernote, it was as close to perfect for my needs as any piece of software I could find.  Unfortunately I had to stop using it due to privacy concerns, and in the years since I've never been able to find a complete replacement for it.

After complaining about this and hearing from others who share a similar need, I've decided to document exactly what I'm looking for as a way to cope with the loss, identify replacements and maybe, just maybe, use as a starting-point for developing my own replacement (if it must come to that).


## What everyone gets wrong

There are many, many alternatives to Evernote, however every one I've tried makes the same mistake: they do a good job with the *note* part, but overlook the *ever*.  

OK that didn't sound as clever as I thought it would.  The mistake is that these applications fail to see that the most valuable components of Evernote are the storage and indexing of documents; the note-taking component is secondary to what makes Evernote so powerful.

_Evernote isn't a note-taking application, it's a personal, private archive and search engine._

## The features

So if we're going to do this right, we need to prioritize features in this order:

1. Privacy, ffs!
2. Store anything, forever
3. Index everything, so you can find it
	1. Full-text search of text-oriented files
	2. OCR image files to make them full-text indexable
	3. Speech-to-text audio & video to make them full-text indexable
	4. Capture context-oriented metadata (upload dates, other free context data, etc.)
4. Provide a search interface beyond "search box"
5. Provide a simple, flexible API for storing and retrieving files & metadata
6. Provide native client applications for all platforms that can upload, download, search and capture data (photos, video, audio, etc.)
7. A nice note taking interface that supports markdown and possibly more, like outlining and other inline information processing (more on this later)


## Great artist's steal

So what existing parts can we use to construct something like this?  Here's some ideas:

* `git` - stores all kinds of files, free version control, but _will it scale???_
* `lucene` - indexing
* `tesseract` - OCR
* `Mozilla Common Voice`- speech-to-text
* `OpenCV` - image analysis


Assuming some or all of those things will work, what's missing?

* A unified API
* Native clients

## Beyond Evernote

What could we add to make this _better_ than Evernote?

* Analysis - ML, etc. to connect files, detect patterns, draw graphs, visualization, etc.
* Backup/redundancy/distribution
* Automatic ingest (think "a bot that swallows your twitter feed", RSS feed, etc.)


# Interested Parties

## Mastodon

trayofbees@innerwebs.social
kelbot@fosstodon.org
sikkdays@mastodon.social
gemlog@mastodonten.de
donblanco@octodon.social
gcupc@glitch.social
anish@vis.social
linuxsocist@icosahedron.website
tao@sunbeam.city
paulfree14@todon.nl
wuhei@fed.wuhei.racing
alanz@social.coop
skiring@mastodon.host
dajbelshaw@mastodon.social


# References

* https://en.wikipedia.org/wiki/Apache_Lucene
* https://github.com/tesseract-ocr/tesseract
* https://research.mozilla.org/machine-learning/
* https://opencv.org/
* https://cmusatyalab.github.io/openface/
* https://standardnotes.org/
* http://zim-wiki.org/
* https://securedrop.org/
* https://www.mailvelope.com/en/faq
* https://lucene.apache.org/solr/