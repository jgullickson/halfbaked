# Overview
Telemetry.io is the next step in the evolution of the general-purpose location service descibed in an earlier note.

# Description
Telemetry.io is an open-source client-server platform for storing and retreiving location and other telemetry data whose primary focus is privacy.  It consists of a centralized server component which receives encrypted telemetry data from client agents avaliable for multiple platforms.  This architecture allows the producer of the data complete control over what data is stored and who has access to it.  

# Value Beyond Platform-Provided Location Tools
By being open-source and platform-agnostic, Telemetry.io is not vulnerable to the systemic weaknesses of vendor-supplied location systems.  By utilizing open-source client and server software, the user is able to inspect the code and guarentee that their information is not being used without their permission.

Applications using Telemetry.io can also avoid the complex process of getting access to existing location and other user data increasingly required by mobile operating systems.  By centralizing these permissions around the Telemetry.io client, partner applications can utilize location data without the added complexity and approval delays associated with using system-provided location services.

# Business Model
Initial revenue channels for Telemetry.io consist of paid user subscriptions and 3rd-party application partnerships.

## Subscriptions
The Telemetry.io business model is simply to provide subscription-based access to store and retieve telemetry data from Telemetry.io servers.  As the client and server software are open-source, users always have the option to operate their own server, however the value-add provided by using Telemetry.io's servers are low-cost (lower reoccuring cost than say a commercial VPC, etc.), zero maintenance and avalibility guarentees.  Additionally users with a Telemetry.io subscription can store and retrieve an unlimited amount of telemetry events at no additional cost (although rate-limiting may apply if necissary).

## Partnerships
In addition to subscription revenue, applications which integrate with Telemetry.io can purchase licenses to use Telemetry.io branding on their products.  This will make applications which utilize location tracking and other privacy-sensitive features more attractive to users who are concerned about privacy and in particular users who are already using Telemetry.io products.

## Operational Costs
The majority of first-year costs will be made up of R&D and software-development expenses.  The second largest cost will be a marketing effort to engage high-profile 3rd party application vendors to secure partnership revenue and raise product awareness.

Ongoing operational costs such as server hosting are expected be minimal during the first year of operation.  The architecture of the system is such that the majority of processing overhead is placed on the client, and the storage requirements for encrypted telemetry data are very small compared to things like media files, etc.  A pair of redundant servers in a single datacenter will be sufficient through the beta stages of development.  An additional pair of servers in a geographically separate datacenter (configured for disater recovery purposes) will be required when the system completes beta testing.

Beyond year one, software development efforts will turn to focus on providing support to developers integrating with the platform (primarilly by developing new clients).  The cost of this effort should be lower than the development and R&D costs of the first year.  Operational costs such as hosting and customer support should scale linearly with the addition of users.  R&D efforts will focus on improving server efficiency to improve scalability to increase subscription revenue profit margins while holding prices and performace steady.

# Additional Notes
Telemetry.io isn't just for phones, its designed to work with any device which logs data.  This allows applications to utilize location and other telemetry data from devices such as computers, cars, bicycles, IoT devices, etc. providing a more wholeistic view of a user's location and other parameters.  Since Telemetry.io is cross-platform, all of this data can be collected and queried together, opening the door to applications that were otherwise cumbersome or impossible with vendor-provided location services. 
