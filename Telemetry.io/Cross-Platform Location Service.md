 
Google Lattitude used to provide a cross-platform application for logging the location of a mobile device and sharing location information with trusted friends.  The objective here is to find a suitable replacement, as well as propose a more general-purpose location service that could be used by mobile phones as well as other location-providing systems.

# Components
The system consists of client and server components.  The server component is expressed as a Unix daemon exposed as a network service.  The client components are platform-specific so a separate implementation is required for each client.

## Server
A Unix daemon which listens on a network port and services inbound requests.  Most likely written in C.

## Mobile Apps
Since the language and the mechanics of gathering location data vary from device-to-device, a device-specific client is required for each supported mobile operating system.

### Android App
TBD

### iOS App
TBD

## Generic Linux Client Service
In addition to the platform-specific mobile device clients, a generic Linux client is provided to add support to devices which run the Linux operating system.  

This client is provided in source-code form since modifications will likely be necissary depending on the version of Linux and the GPS hardware used.
