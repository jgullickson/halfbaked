# Bigfoot Bass Amp

Lunchbox amp for bass guitar

Electronics based on [Ruby](http://www.runoffgroove.com/ruby.html) with "Bassman" modifications.

Housed in a Bigfoot-themed lunchbox with the following features:

* Input jack power switch
* LED "eyes" in the "oo" of "foot"
* Foot-shaped linear volume control (indexed to foot measurement scale)

Components  
  
  * Custom 3d-printed volume knob
  * 1/4" switched phono jack
  * (2) Miniature red LEDs
  * 4" woofer
  * 10k slide pot (volume), 45mm? - https://www.amazon.com/BOURNS-Potentiometer-Travel-Single-Linear/dp/B079ZP3LS5
  * 47n cap
  * 220 uF cap
  * 10 ohm resistor
  * 100n cap
  * 100 uf cap
  * 1n cap
  * 3k9 resistor
  * 1M5 resistor
  * MPF102 FET (alt. 2N5457, J201)
  * LM386
  * 220pF cap
  * 2k2 resistor
  
Notes

Need to figure out the components needed to safely drive the LED's (drop voltage, limit current).  Need to select a suitable speaker for bass guitar, ideally something around 4" to fit well in the space avaliable in the lunchbox.  Need to consider 12v power vs. 9v.