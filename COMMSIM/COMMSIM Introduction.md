 
COMMSIM is a computer simulation of a small community.  The motivation came from the need to test a theory about the scalability of humanity.

After studying a number of autonomous communities (Amish, Native Americans, etc.) a pattern developed that most of these communities deliberately limit their size to about 150 people.  It turns out that there is some research that came to a simular conclusion about the number of people who can be in a group and still genuinely care about each other (citation needed).

So I'd like to experiment with this to see if it is possible to create modern communities (i.e. without rejecting technology or embracing outdated spiritualism, etc.) but it's difficult to carry-out such an experiment in real life.  So the idea of simulating such a community using a computer program came to mind.

I thought about this for awhile but didn't get anywhere on it until I recently re-watched "Wargames" and thought about how it would be fun to put a computer to work simulating things.  Instead of getting hung-up on perfection, I started hacking-out whatever I thought I could make work.

The results of this effort can be found on Github here:

[https://github.com/jjg/commsim](https://github.com/jjg/commsim)

I'll be posting updates to this board as the experiments continue.
