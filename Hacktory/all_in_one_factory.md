One of the most exciting ideas I've had came back to mind while I was watching a documentary about the making of the NeXT computer.  A few years back when I was spending a lot of time working with my 3D printer I had this idea of combining a computer and a printer into a single chassis.  The first design that came to mind was simular to a "classic" one-piece Macintosh, where the face of the computer was hinged and inside was a cavity that housed a 3D printer.

The reason for this was my realization that a tight coupling between the hardware and software used to both design and render 3d objects could result in significant efficiency, and help remove the hurdles associated with getting the separate components to work together.  This is the problem that Apple (when they were at their best) solved better than anyone, and I think it's the right way to eliminate the barriers between users and the work they want to get done.

Today this idea came back to me, and in the intervening years a number of interesting technologies have emerged that make this even more exciting.  In addition to 3d printing, milling could be added to create non-printable parts such as metal gears or even circuit boards.  Pick-and-place and SMD soldering could also be included allowing entire functional circuit boards to be produced by the unit.  With these additional capabilities it's possible to imagine a system that could produce almost anything from a single self-contained workstation.  

Of course this needs to be combined with excellent software and user-interface design to make it live up to the potential of the hardware.  This may be the larger challenge than the hardware itself.  However, there have been significant advances in open-source design software for both electronics and 3d models.  This coupled with the ressuraction of virtual reality may give us some significantly advanced software tools to meet the capabilities of the hardware.

All of this is for naught if it can't be placed in the hands of enough people to change the way we look at the relationship between ourselves and the things we use, so the machine needs to be made in a way that can be accessible financially.  The ideal scenario is that the machine is capable of producing copies of itself; once delivered a single unit (supplied with sufficient raw material) could begin to produce copies of itself to meet local demands.  

This challenges almost all contemporary ideas of creating a market and a sucessful "business", but that's not the goal.  While it is necissary to find a way to make development of this machine sustainable (both the initial R&D as well as on-going support and development) let's not start hamstringing the system immediately by worrying about how to turn it into a company.

# Features

## Hardware

*  3D printing
*  3D scanning
*  Milling
*  Pick and place
*  Solder oven

## Software

*  Collaborative design
*  Object library (shared, searchable)



# References
*  https://www.youtube.com/watch?v=BNeXlJW70KQ
*  http://www.liteplacer.com/

Doug Menuez NeXT documentary
John Nathan NeXT documentary
