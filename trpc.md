# Tufte, Raskin, Pirsig, Cray

We've done things wrong for a long time.  The reason is that we've lost sight of truth and quality.  There's good news: this is easy to change if we re-prioritize the goals of our work.


What do these four people have in common?  I think they are all very attuned to Pirsig's idea of capital-Q Quality.  Not only are they sensitized to Quality, they are uncompromising in their pursuit of it (whether they are aware of this or not).

In pursuit of Quality Tufte and Raskin share a vision of computing that banishes what most people think of when the term "operating system" is used, and instead focuses on getting everything out of the way between the user and their work.  They also share the idea of a document-oriented system that doesn't separate the "means of production" of content.  Their ideas in this area are so similar that I wonder if they worked together, or if this is just an indicator that this approach is where you arrive when pursing the Quality path.

Pirsig laid the foundation for an understanding of Quality.  Coincidently, Pirsig also worked on early digital computers and identified many of the same problems later illuminated by Tufte and Raskin.

Seymore Cray, the grandfather of supercomputing needs little introduction for most people reading this, but his relationship to the other people discussed may be less clear.  Cray clearly was in the pursuit of Quality, so much so that he started and left one company after another as soon as they deviated from the Quality path.


## Let's talk about the stack for a moment

1. Kernel (Linux, BSD, Mach, L4? https://en.wikipedia.org/wiki/L4_microkernel_family, something new?)
2. Chapel execution environment (everything above this line is written in Chapel)
3. Display 