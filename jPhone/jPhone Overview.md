 #jPhone Mark 3

*note: this project needs a new name*

## Overview

This document describes the third iteration of the jPhone project.  The first iteration was an open-source replacement for the iPhone (hence the name "jPhone"), the second was an open-source phone based on Javascript and blockchain technology.  This third iteration is almost a complete departure from the "phone" concept, but retains the handle "jphone" because it serves the same purpose of replacing the mobile phone.

jPhone Mark 3 is an open-source mobile personal computer which consists primarilly of a small system-on-a-chip (SOC) ARM processor and a mobile data network interface module.  This system is paired with a variety of input/output devices connected via Bluetooth, which take the place of a traditional touchscreen interface as the primary method of interaction with the system.  The intention is to explore new methods of interacting with a mobile personal computer beyond the current standard of tapping on a screen.

## Hardware

Two phases are planned for Mark 3 hardware development.  The first phase consists of selecting off-the-shelf, stand-alone development boards that will be combined to create a prototype of the hardware platform.  The second phase will take what is learned from the first, and combine the essential components into a single custom device (this is why it is essential to select open-source hardware for the first phase).

The SOC selected for phase one of hardware development is the Allwinner R8 processor as a part of Next Thing Co's ["CHIP"](http://docs.getchip.com/chip.html#open-source-hardware-where-to-get-it) single-board computer (SBC).  This design was choosen because it is open-source hardware, and contains much of the supporting technology needed for the jPhone (SOC, Bluetooth, WiFi, lipo management, etc.).

The network device selected for phase one is the [Adafruit FONA 3G](https://learn.adafruit.com/adafruit-fona-3g-cellular-gps-breakout/overview) based on the SimComm SIM5320A module.  This unit was selected for its ease-of-use in prototyping and good documentation provided by Adafruit. 

In addition to the cellular module an additional radio data interface is being considered, one that is not dependent on external infrastructure.  The current candidate for this role is the [RFM95W LoRa radio module](https://www.adafruit.com/products/3072) from Adafruit.  This radio's transmit speed is limited to around 19.2kbps but has a range of 2-20km.

## Interfaces

Proposed interfaces include:

* Smartwatch (bitmap display, accelerometer, microphone, etc.)
* Heads-up Display (HUD) (text overlay)
* Smartglasses (speaker/headphone, microphone, tactile input, visual indicator output)
* Fitness monitor (heart rate, blood pressure, pulseox)
* Envionmental sensors (temperature, humidity, pressure)
* Smartgloves (hand location, gestures, tactile feedback)
* Head-mounted Display (HMD) (immersive bitmap display, speaker/headphone, head orientation)
* Bodycams (video input)

## Challenges
One of the primary challenges of this architecture is keeping everything charged.  When the system is composed of many small devices, battery capacity is small as well and keeping track of what is charged and what needs charging will be a hassle.

Perhaps a new way to look at charging is in order, either a method of charging the system as a unit, or some sort of self-charging mechanism.


## References

* https://learn.sparkfun.com/tutorials/serial-peripheral-interface-spi
* https://www.dorkbotpdx.org/blog/paul/better_spi_bus_design_in_3_steps
