I've recently taken an interest in the Forth programming language.  I've looked into Forth before, I think the first experience with it was with a version for the (classic) Mac, and then later when I was learning about Raskin's Cat computer.

Forth is an interesting language in that it's kind of hard to imagine using it for the type of programming I typically do.  Of course I'm just beginning to learn the language, so perhaps there are typical ways to go about writing network and web-oriented software in Forth, but to me it seems so different from the typical structure/object-oriented languages used for these tasks that it's hard for me to imagine writing someting like a REST API in Forth.

That said, it seems ideally suited for something where performance and tight code matter, and where the environment is something new or not defined by existing standards.  This is why I think it might be an interesting fit for the jPhone.

I'm not sure what it takes to implement low-level interfaces in Forth, but it's not hard to imagine what a dictionary might look like to write software for the devices imagined for the current iteration of the jPhone architecture.  These small, semi-autonomos components all contain a little bit of processing power and communicate over a standard protocol (Bluetooth), which is essentially a serial connection back to the main processing unit.  One can imagine each of these running their own Forth programs, capable of operating to some degree independent from one another.  

Forth isn't very far from assembly language, so it might be significantly better at producing programs that run well on these tiny embedded processors, as well as a way to hone the discipline of developers who want to write applications for these systems.

Whether or not it could be practical to write user-level applications in Forth remains to be seen.  It's hard for me to imagine this, but I've only spent about an hour working with the language so I don't have much experience to speak from.  It's also worth noting that much of the Cat's applications (all) were written in Forth, so with the proper foundation, perhaps it's not as strange as it might seem. 
