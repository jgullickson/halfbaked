# simplephone

The simplest device that could replace my smartphone.


## Requirements

  * Make & recieve telephone calls
  * Send & receive SMS messages
  * Provide location information
  

## Components

So far a few parts have been identified which might work well for this project

### Adafruit Feather FONA - https://www.adafruit.com/products/3027

This board combines a cellular module (SIM800), an Atmega32u4 microncontroller and Li-ion battery charger into a single board with all the required support circuitry.  Based on initial research, it looks like this single device can provide all the functionality needed to meet the requirements above.
  


## Notes

To meet requirements, this device will need some form of text input & output.  I really want to avoid using a traditional touchscreen.  Something designed around a chording keyboard is tempting but is a little risky since I don't know how to use one yet.  That said it's probably the most mechanically simple solution to providing full text input capability w/o a touchscreen.

Text-to-speech comes to mind but is far beyond what the ATmega32u4 can manage on its own, and I'd prefer to avoid adding additional processors if possible.  

Looks like it might be possible to do geolocation using [only the cell module](http://www.instructables.com/id/How-to-make-a-Mobile-Cellular-Location-Logger-with/) which would cut down the cost and power budget considerably.

Confirmed that it's possible to get location from the SIM800 module using AT commands.  I don't know how accurate this is, but it's probably accurate enough for this application.

That being the case, the Feather FONA, combined with a battery, OLED display and a chording keyboard should meet all the requirements set out above.  Another advantage of the chording input is that a suitable case could provide a lot of room for the battery, giving the device venerable runtime.

Another plus of the chording input is that it can be fast, making the device potentially usable as a unix terminal.

What's the largest OLED that will fit in the palm of the keyboard?
  * 128x64 white (55.01x27.49)
  * 160x128 color (36x28.7mm)
  * 

I really, really want to use an OLED for this but I just can't seem to find one big enough.  I could use something smaller but that will make some of the UI ideas I had impossible (specifically, labling the finger buttons of the chord keyboard with one-touch selections.  Well, this one might work: https://learn.adafruit.com/2-7-monochrome-128x64-oled-display-module

Looks like 12mm tac switches are "good enough"

An additional feature is that the device could possibly serve as a USB chord keyboard as well...

Not sure if the SIM800 can handle SSL, consider using Speck to encrypt sensitive data (like location updates).

Supposedly the Feather FONA has (some) built-in bluetooth support?


## What can be done NOW?

The parts wishlist for this project is over $100.00.  How much can I do with stuff that I have on-hand?

  * I think I have an Atmega 32u4 board that I could use to prototype the "O/S", etc.
  * I have a TFT display that *might* use SPI; if so it might be able to simulate the OLED module I want to use
  * I have an old IBM keyboard I might be able to harvest keyswitches from
  
With these bits I might be able to cobble-together enough hardware to start writing the firmware even if I don't have the network interface (the cellular modem).  I guess I even have an old Arduino cellular shield that might use an older SIM800 so maybe I could even prototype out the networking code (although the module won't fit inside the device by any means).

I could also prototype the case using these parts so long as I respect the dimensions of the actual components (the microncontroller I have will be smaller, and I think the display will be to, but the keys will be bigger).

If nothing else I might have enough to start learning how to type on a chord keyboard...



## References

  * https://www.adafruit.com/products/3027
  * https://learn.adafruit.com/adafruit-feather-32u4-fona
  * https://www.adafruit.com/products/3133
  * http://www.atmel.com/Images/Atmel-7766-8-bit-AVR-ATmega16U4-32U4_Datasheet.pdf
  * http://www.instructables.com/id/How-to-make-a-Mobile-Cellular-Location-Logger-with/
  * https://learn.adafruit.com/3d-printed-star-trek-communicator/code
  * http://chorder.cs.vassar.edu/featherchorder
  * http://chorder.cs.vassar.edu/
  * http://www.instructables.com/id/Chording-Keyboard-BLE-and-USB/
  * https://en.wikipedia.org/wiki/Speck_%28cipher%29
  * https://github.com/clc/chorder
  * https://www.adafruit.com/wishlists?wid=429997
