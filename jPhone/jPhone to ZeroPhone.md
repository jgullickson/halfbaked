I'm considering making the current/next generation of the jPhone focused on the ZeroPhone concept.

This isn't much of a jump, there was an effort to make jPhone blockchain/DHT-based in the past (bitphone?) and having spent some time with ZeroNet over the last few days has only further established it as embodying many of the technologies I've sought to implement in the past.

There will be a need to address the application layer as it seems unlikely that a full-blown Javascript/HTML application will be suitable for many of the physical interface devices I have in mind for the current iteration of the jPhone, but I don't think that's a show-stopper.  Perhaps some sort of compromise will be suitable.

In any event it's hard to ignore how suitable ZeroNet is for the applications I have in mind, even if it brings with it more of a runtime burden than I had desired. 
