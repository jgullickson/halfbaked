# Hello Operator

A Co-Op Telco

## TODO

* Build server (172.104.140.49)
* Ubuntu
* Firewall
* DNS
* Kamailo
* Website


## Journal

### 10142017

I tried using a tutorial to get up-and-running quickly but that failed (I think due to version differences between the version I installed and the version the tutorial was written for).  So I guess I'm going to have to try a little harder...


### 10162017

First attempt to quickly setup a Kamailio server was unsucessful.  Looks like I'm going to have to actually learn how to run one of these things (and its dependencies) before I can start experimenting with it.

Starting over with a fresh Debian 9 VM and this tutorial: http://kamailio.org/docs/tutorials/devel/kamailio-install-guide-deb/

```
apt update
apt install mysql-server
apt install kamailio kamailio-mysql-modules
apt install kamailio-tls-modules
```

After combining [this Debian tutorial](http://kamailio.org/docs/tutorials/devel/kamailio-install-guide-deb/) with [this TLS tutorial](http://www.fredposner.com/1836/kamailio-tls-and-letsencrypt/) and troubleshooting Unix permissions issues on the certificates, hello.2soc.net server was running without errors.

After creating a few accounts, I installed `ufw`, opened port 5061 and tried connecting a few clients.

After a couple hours of troubleshooting I was able to get audio and video calls through (mysterious auth problems).  I also had to drop the firewall to get the video working so I'm sure there are some ports I'm missing from my config.

To be safe I re-enabled the firewall on hello.2soc.net until I can spend some more time figuring out what ports should be open and what can be locked-down.


### 10172017

I finally have a combination that appears to work for text, voice & video (I'm still having some issues with video but I think that might be client-related).

Here's a summary of the steps

1. Create Debian 9 VM on Linode
2. Install packages: `apt-get update && apt-get install ufw mysql-server kamailio kamailio-mysql-modules rttproxy`
3. Configure firewall: allow tcp 5061, allow udp 35000 - 65535
4. Configure rttproxy: 'rtpproxy -l 172.104.140.49 -s udp:127.0.0.1:7722 -u rtpproxy rtpproxy'
5. Install Let's Encrypt: http://www.fredposner.com/1836/kamailio-tls-and-letsencrypt/
6. Create certificates: (see above link)
7. Configure Kamailio: http://kamailio.org/docs/tutorials/devel/kamailio-install-guide-deb/

Most of the Kamailio configuration is covered in the link under item 7 above other than NAT/rttproxy and TLS.  The link under item 5 covers TLS and all that appears to be needed for NAT is to add `#!define WITH_NAT` to the top of `kamailio.cfg`.

I'll break this outline into detailed steps soon.



## References

* https://www.kamailio.org/w/
* http://kb.asipto.com/kamailio:skype-like-service-in-less-than-one-hour
* https://www.linode.com/docs/security/firewalls/configure-firewall-with-ufw
* http://kamailio.org/docs/tutorials/devel/kamailio-install-guide-deb/
* http://www.fredposner.com/1836/kamailio-tls-and-letsencrypt/
* https://richardskingdom.net/howto-kamailio-sip-proxy-nat-debian-wheezy
* https://stackoverflow.com/questions/29637142/raspberrypi-making-sip-outbound-calls-using-linphonec-or-an-alternative-sip-sof
* http://www.pjsip.org/
* https://www.raspberrypi.org/forums/viewtopic.php?f=44&t=73645&p=532692
* https://stackoverflow.com/questions/29435648/sip-client-for-raspberry-pi-that-works-from-command-line
* https://en.wikipedia.org/wiki/Analog_telephone_adapter
