# Reelbot

For a few years I've wanted to build a robot lawnmower.  Not for any practical reason (our lawn is not big enough to demand it), but mostly because I like robots.

I've seen a few robot lanwmowers but for the most part they are large, kind of like remote-control riding lawmowers.  What I have in mind is something more compact.

Since about a year after we've owned our house we've used a "reel" mower.  We started out with a typical gas mower, but the yard was so small that it was overkill, and it took up a lot of space in the garage.  It also require fuel, oil and a lot more maintenace than the reel mower does.  Plus reel mowers just look cooler, all those spinning parts and blades and such...anyway

This led me to design a robot lawnmower around a reel mower design instead of the "deck" or riding mower designs more common today.  

The design is rather simple: take an existing reel mower, remove the handle and add two motors; one to drive each wheel.  This provides both drive and steering, tank-style.

Having taken-apart our mowers several times for cleaning, sharpening, etc. I noticed that the inside of the wheel is a gear which in turn drives a pinion gear that spins the reel.  This makes it very easy to interface a drive motor to the wheel by simply creating another gear like the reel drive gear that is attached to an electric motor.  Looking at this particular mower (a Scots XYZ), there appears to be enough room to mount a suitable motor to the flange which protects the gear-side of the wheel from the outside world, so this approach is at least theoretically possible.

The first job is to model a suitable drive gear.  For this I turn to OpenSCAD.  There is a gear library which I've used before and works very well for creating bespoke gears.  

A better engineer could probably do this in one take, but for me an iterative approach is prefered.  First, I'll try to model a copy of the original gear.  Then, when I know I can make a copy I can tweak it to match the motor/gear ratio/etc.

The downside to the iterative approach is that it requires running off a print of each variation, which takes time.  I don't mind this though because I have other problems to solve while I'm waiting for the printer.

Along with the practical limits on how few teeth the gear can have (and still function properly), there is a mechanical limit based on the position of the motor on the flange that covers the gear side of the wheel.  The flange has a bevel or flare, and the motor will need to be mounted in such a way that it mounts against the flat, unflared part of this flange.  This puts another limit on how small the drive gear can be.  Some additional experimentation may be in order to determine the balance between these constraints.

In the next installment I'll be iterating on a gear design that meets these criteria (along with mating to the selected motors) and exploring power & control systems.

As always design files and code are open-source and can be found on Github here: [https://github.com/jjg/reelbot](https://github.com/jjg/reelbot).

## Part 2

Once a suitable drive gear has been created a motor needs to be selected.  There's a lot of things to consider when selecting a motor for a robot, but my starting point is almost always dependent on what I have on-hand.  Today what comes to mind are a pair of RS-540 motors that currently live inside another robot (Sux0rz).  

After Makerfair Milwaukee, I decided that Sux0rz drivetrain needed improvement, so I plan on replacing these motors anyway.  Also they are a fairly matched pair which is important in this sort of a drive system.  If nothing else, I already have them (and a matched set of motor controllers) so if they don't work out I'm not out anything and I can start making progress today instead of waiting on parts.

The motor controllers are commodity RC electronic speed controls, so for initial testing computer control isn't required (a basic RC transmitter and receiver will suffice).  I'm not sure exactly *how* autonomous I want this machine to be after all, so I'll probalby start with RC control (assuming I can find a suitable transmitter/reciever pair in the lab...).

