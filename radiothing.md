I have a great little radio now but there's not always something great *on* the radio.  When that happens I'd like to have a way to play things I want to listen to that is just as convinient as listening to the radio.

What I have in mind is something like a little device I can attach to the AUX input on the radio that will always be playing something I want to listen to.  This could be as simple as an iPod shuffling a collection of music I like, but given the difficulty of dealing with iPods that's not an option.

It could be a cheap filesystem-based music player, but most of them have bad analog stages.  This device doesn't have to be "hi-fi", but something that sounds bad is going to grind on me, and something that produces lots of electrical interferance/noise/etc. is going to ruin regular radio playback as well.

So I end up thinking about what I can do with the parts on-hand, and that usually amounts to some single-board computer.  There's a million ways to stream music to something like a Raspberry Pi, and I have a few old Pi's laying around now that most of my work focuses on other hardware, but I want to avoid streaming because it's prone to playback issues, requires additional infrastructure (a server) and the WiFi reception is poor where the radio lives so it's likely to experience playback interruptions, etc.

It would be cool if I could control the content on the device without having to build a physical interface into it, and like I said I want using it to be as simple as using the radio.

What comes to mind is a device that subscribes to a feed like a podcast.  Something that can opportunistically download audio from a playlist controlled elsewhere, but that doesn't require network access to play the downloaded audio.

I poked-around a bit looking for a Linux command-line podcast client and found a few interesting things.  It seems like most of them focus on managing the feed vs. playback, but for my application that's probably OK.  One piece of software could be reading the feed, downloading media and stuffing it in some directory, while something else is continually shuffling the contents of that directory.

For example, I might be able to use [greg](https://github.com/manolomartinez/greg) to manage the feed and download new media, and then use [mpd](https://www.musicpd.org/) to continually playback the downloads.

Now that I think about it [SyncThing](https://syncthing.net/) might be an easier way to manage the pool of media on this device.  This wouldn't require any server-side infrastructure and might allow for less configuration as well.  It certainly solves management complexities like firewall traversal and while it requires a piece of client software for management, I already use SyncThing elsewhere so it's not much additonal trouble. 


# 10162018

I setup an old Raspberry Pi (original model b?), hostname `radiopal.local` and here's the plan:

* Install & configure Raspbian Stretch Minimal
* Install MPD
* Get playback working over analog (headphone out)
* Install syncthing & sync with laptop
* Come up with a way to make sure MPD knows when syncthing delivers new files

If I can get this far I should have the *functionality* needed.  Next will be to put it in a reasonable case and provide some feedback & control (how full the disk is, maybe play/pause/skip, etc.).

I'm thinking about doing syncthing first just because it's hard to test playback where I have the Pi setup right now.  I guess could test with headphones...

Steps:

* `speaker-test` 
* `sudo apt-get install mpd`
* `sudo apt-get install vim`
* `sudo vim /etc/mpd.conf`
* from laptop: `scp *mp3 pi@radiopal.local:/mp3`
* `chgrp -R audio ./mp3`
* `sudo systemctl restart mpd`
* `mpc update`
* `mpc add <file.mp3>`
* `mpc repeat`
* `mpc random`
* `mpc play`
* `mpc status`

This works, so now to add syncthing:

* `sudo apt-get install syncthing`
* `syncthing`
* `vim .config/syncthing/config.xml`
* Change <address> under <gui> section to 0.0.0.0:8384

Log-in to the web interface, setup a password and share the music directory.  Add another device (in this case, my laptop) and share the music directory with the new device.  Once synchronization catches-up, try syncing from laptop -> Raspberry Pi and if that works start configuring syncthing to run like a proper service:

* `sudo apt-get install git`
* `git clone https://github.com/syncthing/syncthing`
* `cd syncthing/etc/linux-systemd/system`
* `sudo mv syncthing@.service /etc/systemd/system/syncthing@.service`
* `sudo systemctl enable syncthing@pi.service`
* `sudo systemctl start syncthing@pi.service`

...

Now that playback works and we can sync music, we need a way to automatically tell MPD to play any new stuff.  There's probably a more elegant solution, but some basic crontab entries should be good enough:

`*/5 * * * * /usr/bin/mpc update`
`*/5 * * * * /usr/bin/mpc ls | /usr/bin/mpc add`

The old single-core Pi struggles a bit running syncthing & mpd at the same time (unix load > 1) but playback of flac files doesn't seem to be impacted.  We'll see how much this drops when syncthing isn't actively synchronizing files.

When syncthing isn't synchronizing, utilization drops to ~1% CPU or less.

At this point if the system is rebooted I think everything should start back up automatically, and based on what I've read about MPD, playback should resume (since it was playing when it was shutdown).

Confirmed that after reboot, everything starts back up, including playback!


## Next Steps

* Prevent the disk from getting so full it crashes the device
* Provide an interface to display storage utilization
* Provide a physical interface for basic playback control (play/pause, reject)
* Handle shutdown gracefully (provide a control or some other safe power-down handling)
* Add a WiFi interface so the device can sync without Ethernet
* Try adding the "Pi-rate Radio" thing and see if we can broadcast the music

Notes:

* music directory is /var/lib/mpd/music
* after copying music files, need to run `mpc update` and `mpc add *.mp3`
* 

# 10182018

Ran in "offline mode" (no network connection) all day today with no playback problems.  Even had a couple "unexpected shutdowns" (i.e., lost power) with no adverse effects (although I'm sure with enough of them the SD card is going to get corrupted).


# 10232018

Experimenting with Shoutcast for a really silly project.  Got this working to stream MPD output to the Shoutcast server:

```
audio_output {
  type        "shout"
  encoding    "mp3"
  name        "station name"
  host        "shoutcast.server.com"
  port        "8000"
  mount       "/mpd.mp3"
  password    "secret"
  bitrate     "128"
  format      "44100:16:1"
  protocol    "shoutcast"
  description "A shoutcast station"
  url         "http://your.shoutcast.server.com"
  genre       "genre isnt real"
  public      "no"
  timeout     "2"
}

```

This works well with a basic Shoutcast server setup.  Running this alongside local audio output (the existing analog audio out) pushes CPU utilization to 50-60% but doesn't appear to impact playback (no drop-outs, etc.).  That said I think based on the utilization I've seen when syncronizing music, a beefier board is a good idea, and a contemporary Raspberry Pi should be more than enough.


# References

* https://gist.github.com/zentralwerkstatt/b94ab44938fbdc8b957c674bf9261954
* https://www.tecmint.com/install-shoutcast-in-linux/
* https://download.nullsoft.com/shoutcast/tools/
* http://wiki.winamp.com/wiki/SHOUTcast_DNAS_Server_2#Configuration_File
* https://www.musicpd.org/doc/html/user.html?highlight=audio_output
* https://www.musicpd.org/doc/html/user.html?highlight=audio_output#encoder-plugins
* https://www.musicpd.org/doc/html/user.html?highlight=audio_output#encoder-plugins
* https://www.raspberrypi.org/magpi/off-switch-raspberry-pi/
* https://github.com/TonyLHansen/raspberry-pi-safe-off-switch/
* https://gpiozero.readthedocs.io/en/stable/
* https://www.raspberrypi.org/blog/gpio-zero-a-friendly-python-api-for-physical-computing/
*
