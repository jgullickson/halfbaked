# openrino
An open-source clone of the Garmin Rino GPS+Walkie-talkie.

## Overview

The Garmin Rino combines a walkie-talkie radio with a GPS receiver.  This not only reduces the number of devices you need to carry, but it also has the unique feature of being able to send your location to another person with a Rino device.  This is something we take for granted with smartphones but unlike smartphones these devices don't require a carrier network to function.  This makes them very useful in areas where cellular carrier network access is sparse or non-existant, or under conditions where using a carrier network is not desirable.

These features of the Garmin device make it very useful for a number of scenarios, however the device is designed for serious outdoor use which limits its audience somewhat.  This results in a device that is larger, heavier and more expensive than it needs to be for other less-strenuous applications.  Additionally it appears that the device is proprietary (or at least effectively so), as there are no other simular (and compatible) devices offered by other companies.

The purpose of this project is to design and produce an open-source alternative to the Garmin device that provides some of the functionality in a more accessible package.

## Hardware

### Radio
Instead of the FRMS-type radio used in the Rino, there are a number of low-power, long-range digital radio devices that could be used for this application.  Most of these radios are not intended for audio data use however the quality required for basic half-duplex voice communication is low enough that these inexpensive radios might suffice.

### GPS
Any inexpensive GPS receiver should be adequite.  Small size and low power consumption are ideal as are powerful antennas.

### MCU
Some sort of processor will be needed to run the device firmware and manage communication between the radio, GPS and user interface components.  Depending on the design of the user interface this could be a fairly simple 8-bit MCU or a more sophisticated SOC/SBC running a full-blown operating system such as Linux.  Ideally it will be inexpensive, compact and low-power.

### User Interface Components
Some form of user interface will be required.  A hardware-based interface is preferable to reduce the complexity of the device and increase its durability (since it may be used under averse conditions).  It's yet to be determined if a fully-functional UI can be implemented using hardware controls and simple feedback mechanisms (LEDs, audio cues, etc.) or if something with a full-blown display, etc. will be required.


## Software
Minimally, the device software needs to provide the following features:

*  Device power on/off
*  Indicate device status (power, send/recieve state, battery state)
*  Push-to-talk
*  Send location
*  Indicate received location
