# Smart First Aid Kit

A typical first aid kit containing typical first aid supplies but with the addition of a computer and sensor package capable of capturing and logging statistics about the injured person and providing information and guidance on treating their injuries.

## System Components

### Computer Hardware

An embedded computer consisting of a touch-screen interface an array of sensors which can be connected to the injured person. 


### Computer Software

The computer's software is capable of logging raw sensor data and accepting user input in the form of direct interaction with the display, spoken audio commands and other contextual input based on geographic location, network avaliability, etc.  The computer's software includes  in-depth medical references, an interactive first-aid troubleshooting tool and machine intelligence capable of analyzing all input data to guide treating the injured person.  The software is also capable of contacting emergency response services via the network interface if a network is avaliable and communicating the condition of the injured person without requiring the attention of the person treating the injured person.


### Network

A network interface capable of contacting emergency response services and communicating any data logged about the injured person.


### Supply Tray

A supply tray with individual compartments containing typical first-aid supplies.  Each compartment contains an indicator which is used by the computer to indicate which of the supplies is being referred to by on-screen or spoken instructions.  Each compartment is capable of detecting when its supply level is exausted, allowing the computer provide feedback that the supply needs to be refilled (optionally re-ordering the supply automatically) and suggest alternatives to the depleted supply during treatment.


### Case

A tough sealed case which contains the computer, sensors and supply tray.  The case is waterproof and boyant, can be opened one-handed and indicates computer power and resupply status externally.


### Other Components & Notes

All external system components are sealed against moisture, grit and other environmental factors which are common in emergency situations.