# Neutralizer

A net neutrality implemetation in hardware.

## Description

The Neutralizer is a hardware device that sits between a user's Internet connection (cable modem, DSL, etc.) and their router/Wi-Fi bridge/etc.

[cable modem] --ethernet--> [Neutralizer] --ethernet--> [Wi-fi bridge]

Once connected, the Neutralizer automatically establishes a VPN connection to the Neutralizer server farm and presents this connnection to the user's router/bridge as the only upstream connection to the Internet.  This is presented via DHCP, allowing the router/bridge to auto-configure as if it were directly connected to the ISP's device.

From this point forward all of the user's traffic is routed through this tunnel and appears to originate from Neutralizer's servers, thus evading the ISP's attempt to shape or prioritize traffic.

In addition to VPN, the Neutralizer provides additional network services to enhance privacy and performance:

  * A local caching DNS server
  * Pi-Hole ad blocking (if we can get permission)
  * Other stuff?

These additional features (as well as the VPN) can be enabled/disabled using physical switches on the Neutralizer device itself.  No software/web/etc. config is necissary to configure or use the Neutralizer.


## Hardware

The Neuralizer hardware consists of a compact single board computer containing an ARM processor and two Ethernet interfaces.  Ideally the network interfaces would be gigabit or higher and will be connected in such a way as to avoid hardware bottlenecks in moving data between them.

*note: gigabit may be overkill since most consumer Internet connections are bandwidth-limited far below gigabit speeds.*

Additional hardware may be advantageous in order to more efficiently encrypt/decrypt the VPN connection.  This may take the form of additional chips or perhaps an FPGA implementation on a hybrid ARM/FPGA cpu module.

A design which utilies an off-the-shelf SBC (even if not optimal) would be ideal in that it would provide a practical DIY path for users who want to build their own Neutralizer using the open-source software base.  This also provides significant advantages in delivering the first release of the device to market, etc.


## Software

Linux and open-source packages such as OpenVPN can provide the software features of the Neutralizer.  What remains to be developed are the automation components which configure and run these packages automatically and interface with the hardware control mechanisms.

In addition, automated maintenance components will need to be selected or developed to ensure the underlying system software and operating system can be kept up-to-date.

Alternatively another O/S (FreeBSD, for example) may be selected if it can provide the same functionality with lower hardware overhead or greater security.


## Service

In addition to the client-side hardware and software there is a service component to the Neutralizer in the form of servers and software which provide the public Internet side of the VPN connection.  The cost of procuring and operating these systems needs to be supported by the Neutralizer user in some way either directly through a service charge, or indirectly through device margin, voulentary donaions, co-op, etc.


## Open Source

Both the hardware and the software created for the Neutralizer will be open-source.  The device may contain non-open-source components (for example some ARM SOC's are not open-source) but an effort will be made to make the device as open as possible within the constraints of creating a reliable device which can be delivered at a cost that is acceptable to the user.  It is expected that any non-open-source components will be phased-out in favor of more open alternatives in each design iteration.

There are numerous reasons for keeping the device open, some of the most important ones are allowing users to build their own devices, allowing for independent, 3rd-party security evaluations and transparency in the handling of user's Internet traffic.

Whether or not the server/service-side of the software stack can be made open-source will depend on further analysis.  The key concern here is whether or not shareing the code would present a risk to user's privacy.  I would assume that in a properly-designed system this would not be the case, but a this stage in the development of the system I can't say with complete confidence that it will be possible to share the code deveoped for the server-side of the system without introducing risk to users.


## Open Questions

One immediate question is how much hardware will be required to support the always-on VPN.  I would like to express this in terms that are in relationship to bandwidth (for example, Mhz-per-Megabyte) so a "sweet spot" can be selected based on the most common network performance envelope for the audience.  This of course will depend somewhat on what hardware features are avaliable to improve performance of the software selected.

Another question is whether or not a suitable SBC exists at a price point that is within the desired range.  What the desired price range is will be defined somewhat by answers to the questions raised in the "Service" section above, but a total unit cost of < $50.00 seems like a good guess for now.

Additional research needs to be done to ensure that combining VPN with DNS is enough to evade ISP muddling.  I can't immediately imagine how they would do it (other than just suppressing all VPN traffic) but it would be good to prove this before banking the entire project on that being true.


## References

  * https://www.anonabox.com/buy-anonabox-tunneler.html
  * https://openvpn.net/
