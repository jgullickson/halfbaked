# Saturn Diamond

A new personal computer inspired by the Jupiter Ace.


## Overview

The Saturn Diamond is a small, self-contained computer consisting of a keyboard, display, battery and a single yet-to-be-named I/O port.  The system is centered around the STM32F103 microcontroller and uses FORTH as both its programming language and operating environment.


## Hardware

The system hardware consists of a CPU module, keyboard, display, battery, battery charge controller and a single I/O port.


### CPU Module

The heart of the hardware is the STM32F103 chip.  This chip mounted on a board that is essentially a clone of the Maple Mini.

**STM32F103 Specifications**

| Why | Header?|
| --- | --- |
| CPU | ARM Cortex M3 |
| Clock | 72MHz |
| RAM | 20 kB (static)|
| Flash | 128 kB|
| Power | 2.0-2.6V native (5V using board's regulator) |


### Keyboard

### Display

### Battery & Charge Controller

### I/O Port

The I/O port is an external port consisting of all remaining unused pins on the STM32 module, plus some standard stuff like power, ground, etc.


## Software

The system software is FORTH (likely Mercrisp).


## Notes

I need to come up with a clever name for the I/O port.

It's not clear at the moment if it is better to try and use the STM32 itself to drive the display, keyboard, etc. or if dedicated terminal hardware is a better choice.  The advantage to using the STM32 is a reduced parts count as well as complete software control over these functions from FORTH.  The disadvantage is increased software complexity and a significant reduction in the amount of GPIO for the I/O port.

I'm leaning toward the STM32-only solution if only because it puts the work into the software domain, which is more my speed and feels like something I have more control over.  It also reduces packaging complexity which is something I have a new level of respect for since the Open Digial 8 project.

Going the STM32-only route is likely going to lean on FORTH's cooperative multitasking, and depending on how much overhead that involves, it might eat up too much of the very limited memory available on this chip.  It looks like this decision is going to require more thought, and perhaps the answer will assert itself once I'm actually working on the hardware.

Regardless of which way that choice falls, I'm leaning toward a character-based display vs. something bitmapped.  The obvious reasons are that it requires a lot less resources to do a character display (especially if hardware external to the STM32 is involved) and it's going to be faster as well.  I don't want to hamstring the applications for this computer but everything I have in mind now is compatible with a character display, especially if you add some character-based graphics (think ANSI or PETSCII) to the mix.

This also makes the display hardware itself simpler and less expensive, and probably more power-stingy as well.

I might change my mind (especially if I decide to go the STM32-only route to drive the display) but for now I'm definately leaning toward a character-based display.



## References

* https://en.wikipedia.org/wiki/Jupiter_Ace
* https://en.wikipedia.org/wiki/Canon_Cat
* https://hackaday.com/2017/04/19/moving-forth-with-mecrisp-stellaris-and-embello/
* https://hackaday.com/2017/04/26/making-a-solar-cell-tester-with-mecrisp-stellaris-forth/
* http://www.st.com/en/microcontrollers/stm32f103.html?querycriteria=productId=LN1565
* https://en.wikipedia.org/wiki/Stack_machine
* https://en.wikipedia.org/wiki/Forth_(programming_language)
* http://mecrisp.sourceforge.net/
* https://github.com/hexagon5un/hackaday-forth
* https://github.com/rogerclarkmelbourne/Arduino_STM32/wiki/Maple-and-Maple-mini
* http://embello.jeelabs.org/flib/
* https://github.com/texane/stlink
* https://github.com/jeelabs/embello/releases
* https://github.com/jeelabs/embello/blob/master/explore/1608-forth/flib/stm32f1/io.fs
* https://github.com/jeelabs/mecrisp-stellaris/blob/master/common/multitask.txt
* http://www.ebay.com/itm/5V-40x4-4004-LCM-Monochrome-Character-LCD-Display-Module-w-Tutorial-HD44780-/291024701200?hash=item43c26c8710:g:j9wAAOxyRNJSkvg4