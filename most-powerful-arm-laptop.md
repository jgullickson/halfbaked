# Worlds Most Powerful ARM Laptop	

Clearly I don't have the resources to build the most powerful ARM supercomputer (not even powerful enough to break the top500), but I might be able to build the most powerful ARM-based laptop by repackaging RAIN Mark II into a portable chassis.

This is something I've considered for quite awhile, and I've even sketched-out a few ideas, but I never took it too seriously until recently.  Now that I've had a few people ask about it, and I realized that it's likely such a machine would be the most powerful ARM laptop created, I'm looking more seriously into it.

Switching gears like this will also help resolve (or at least neutralize) some internal debate I'm having about what the next steps for Mark II look like.  In some ways the current form factor has met it's design goals (to measure an ARM-based cluster against the performance of the Intel-based Mark I machine), so it might be time for something new.

Let's start with the electronics (because that's easier to explain without pictures).

## Two ways to go

Both directions feature the Clusterboard, but they depart a bit from there.

The first way is to use a the same PINE A64 LTS front-end node as the current Mark II, then add an LCD, battery, charging circuitry and potentially port extension/expansion and cram it into a portable case.  This is well-known territory but it does put some constraints on the design given the dimensions of the A64 board.

The second way is to "recycle" Pinebook components to create the front-end.  The upside of this approach is that the physical dimensions are suitable for a portable machine, power management is built-in and the two-part design would make it easy to keep the ports exposed without custom work.  The downsides include the fact that there's no Ethernet port (there may be pins but the port itself is absent), and it's unlikely that the power supply is capable of delivering enough current to feed the Clusterboard.

The other downside to the "recycling" route is that you can't purchase Pinebook parts in bulk, so re-creating this design won't "scale" the way the more "traditional" design will.  I don't know if scaliable production is even a factor, but it's worth mentioning.

## Common concerns

Regardless of which route is taken there is the matter of a custom case, a keyboard and some sort of pointing device.  

I've always imagined this machine as something akin to the GRiD Compass or original Macintosh Portable (which looks a lot like an Apple //c with an LCD bolted-on).  This includes a "traditional" keyboard with Cherry-MX or simular buckling-spring switches.  What I haven't decided on is the pointing device, and this has been a struggle for me.

Personally, I avoid using a pointing device as much as possible, but when I need one, I prefer a mouse.  If not a mouse then I prefer the Thinkpad's "trackpoint", and at the bottom of my list is the trackpad.  Part of the reason that the trackpad is at the bottom of my list is that it has to be very, very good to be tolerable and unfortunately the only one's I've used that I would consider this good are on Apple machines.  Most of the ones I've used with Linux are bad or worse, so it's hard to justify all that cost and complexity when a $10 Amazon Basic's mouse works better than even the best trackpad.

The other problem with trackpads is that they can double the amount of space needed for the keyboard area.  To add a trackpad to the designs I've made so far would make the entire machine absurdly long (it's already pretty long).

But a mouse really isn't suitable for a laptop, so something else is going to be necessary, and I'm going to propose something that never occurred to me before a week or two ago when I attended an Edward Tufte class: a touchscreen.

Hear me out.  I have issues with touchscreens, they get dirty, and they pretty much demand that you take your hands off the keyboard to use them, but as an alternative to a big, low-quality trackpad it might be a fair trade-off.  Add to this that a touchscreen makes possible the elimination of all sorts of "administrative debris", as Tufte would put it, and it's something worth considering.  This consideration is compounded by the fact that the Pine hardware has built-in support for touchscreens, and one of the LCD's I'm considering for this design comes with a touchscreen digitizer anyway and it becomes a somewhat obvious choice.

